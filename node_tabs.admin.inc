<?php

error_reporting(E_ALL);
ini_set('display_errors', true);

/**
 * @file
 * Administration interface for the node tabs module.
 */

function node_tabs_administer() {
  $form = array();
  
  // Fetch all the content types.
  $raw_types = node_get_types();
  $content_types = array();
  
  // Build the options list from the content types list.
  foreach($raw_types as $key => $value) {
    $content_types[$key] = $value->name;
  }
  
  // List all the content types using checkboxes.
  $form['node_tabs_content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Choose content types'),
    '#description' => t('Select the content types you want node tabs associated with.'),
    '#options' => $content_types,
    '#default_value' => variable_get('node_tabs_content_types', array()),
  );
  
  return system_settings_form($form);
}
